//====================== FUNCTION =================== 1

// "function" untuk mendeklarasikan fungsi dan di dalam scope function ada kode/perintah yg di eksekusi.
// return untuk mengembalikan hasil

function diskon(x) {						// <--- scope diskon()
	let musimPandemik = (x * 30) / 100		// <--- scope diskon()
	return musimPandemik					// <--- scope diskon()
}
let sale = diskon(20000)
console.log(sale);

//=================================================== 2

// Parameter harus relevan dengan fungsinya. 
function sayHiTo(name) {
	let halo = `Hai ${name.toUpperCase()}!` // <--- Proses parameter yg akan di anggap string
	return halo
}
let test1 = sayHiTo('everything')	// <--- Parameter pada fungsi harus string juga
console.log(test1)

//let test2 = sayHiTo(100)   // <--- Parameter dengan beda tipe data akan menghasilkan error
//console.log(test2)         // <--- Buka Comment line 24 dan 25 jika ingin mencoba

//=================================================== 3

// 3 Cara untuk mendeklarasikan "function"
// Function Declaration (ES5)
function volTabung(r,t) {
	return 3.14 * r**2 * t
}													//		 Bisa juga di tulis
console.log(`Volume tabung : ${volTabung(10,4)}`);  //  <--- console.log("Volume Tabung :" + volTabung(10,4));

// Function Expression
const luasPersegiPanjang = function(p,l) {
	return p * l
}
console.log(`Luas Persegi Panjang : ${luasPersegiPanjang(10,4)}`);

// Arrow Function
const kelSegitiga = (a,b,c) => a + b + c	// <--- function lebih simpel, jika hanya satu baris tidak perlu "return"
console.log(`Keliling Segitiga : ${kelSegitiga(2,5,4)}`) ;  


//====================== CLASS =================== 4

class Person {		// <--- keyword "class" di ikuti dengan nama class ("Person")
	constructor(name, address) {
		this.name = name;
		this.address = address;
	}
}


// Contoh Penulisan Class dan Alur Aksesnya
class Human {		// <--- (1)
	static isLivingOnEarth = true ;		// <--- Static Property		(2)
	constructor(name,address) {		// <---Constructor Method		(3)
		this.name = name;	// <--- deklarasi constructor
		this.address = address;		// <--- deklarasi constructor
	}
	introduce() {		// <--- Buat Method untuk class Human		(4)
		console.log(`Hi, My name is ${this.name}`)
	}
}
console.log(Human.isLivingOnEarth)		// <--- output 'true'

Human.prototype.greet = function(name) {		// <--- Prototype / instance method		(5)
	console.log(`Hi, ${name}, I am ${this.name}`)
}
Human.destroy = function(thing) {		// <--- Static Method		(6)
	console.log(`Human is destroying ${thing}`)
}

let mj = new Human('Michael Jackson', 'Isekai');		// <--- Instance human create new object		(7)
console.log(mj);		// <--- output "human { name: 'Michael Jackson', address: 'Isekai' }"
// alurnya (1) --> (3) --> (7)

console.log(mj instanceof Human)		// <--- output "true"
// alurnya (7) --> (1)

console.log(mj.introduce())		// <--- output "Hi, My name is Michael Jackson"
// alurnya	(7) --> (1) --> (4)

console.log(mj.greet('Donald Trump'));		// <--- output "Hi, Donald Trump, I am Michael Jackson"
// alurnya (7) --> (5) --> (parameter greet di line 86)

console.log(Human.destroy('Amazon Forest'));		// <--- output "Human is destroying Amazon Forest"
// alurnya (6) --> (parameter destroy di line 89)


//====================== ENCAPCULATION PUBLIC =================== 4

class orang {		// <--- Awal Scope Class
	constructor(nama, alamat){
		this.nama = nama;
		this.alamat = alamat;
	}
	kenalin(){		// <-- Public Method ini bisa dipanggil diluar class
		console.log(`Hello, Nama saya ${this.nama} `)
	}
	static isEating(food) {		// <-- Public Method ini bisa dipanggil diluar class
		let foods = ['plant','animal'];
		return foods.includes(food.toLowerCase());
	}
}		// <--- Akhir Scope Class

let ch = new orang('Chandra santoso','Bekasi');
console.log(ch)
console.log(ch.kenalin());		// <--- Di panggil diluar class
console.log(orang.isEating('plant'))		// <--- Di panggil diluar class
console.log(orang.isEating('orang'))		// <--- Di panggil diluar class

//====================== ENCAPCULATION PRIVATE =================== 4

class Pribadi {		// <--- Awal Scope Pribadi
	constructor(password, aib) {
	this.password = password;
	this.aib = aib;
	}
	#ceritaKeCewe = () => {		// <--- Private Method 1
	console.log(`My aib will become public ${this.aib}`)
	}
	ngobrol() {
	console.log(this.#ceritaKeCewe());		// <--- Private Method 1 nya di panggil
	}
	static #isHidingArea = true;		// <--- Private Method 2
	}		// <--- Akhir Scope Pribadi

let rhs = new Pribadi("1234", "fotoTelanjang");		// <--- assign dan declare
console.log(rhs.ngobrol())		// <--- Output: My aib will become public fotoTelanjang
	// Bisa di panggil karena Private Method 1 sudah di panggil terlebih dahulu di dalam scope.

	// try {
	// Pribadi.#isHidingArea		// <--- akan Error
	// rhs.#ceritaKeCewe()			// <--- akan Error
	// }							// <--- Jika ingin mencoba hapus comment di line 135 - 138
	
	// Karena Private Method 2 di panggil langsung dari luar scope
	// Private field '#isHidingArea' must be declared in an enclosing class
	
//====================== ENCAPCULATION PROTECTED =================== 4

