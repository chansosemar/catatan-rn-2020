//================== Variable ================

//RULES Variable
// 1. Nama harus mengandung huruf, angka atau simbol ( $ , _ )
// 2. Karakter prtama variable tidak boleh angka

//================== SCOPE ================
// 2. LET & CONST 
// 1. VAR 
var diskon = 500 // <--- global scope
if (true){
    var diskon = 300 // <--- global scope
}
console.log(diskon); // output menjadi 300
                    // karena var adalah global sscope

        // Var bisa di ubah menjadi local scope dengan memakai function scope
       
        // Global Scope --> Function scope = Local Scope

        var diskon = 500  // <--- global scope
        function diskonscope() {  // <--- function
            var diskon = 300    // <---local scope
            console.log(diskon)  // <--- output 300
        }
        diskonscope()
        console.log(diskon); // <--- output 500

// 2. LET & CONST 
// Const nilainya tidak akan berubah dalam kondisi apapun.
// Let dan Const bersifat block scope variable hanya bisa di akses didalam scope (local scope)

let jumlah = 500 
if (true){  // <--- awal scope
    let jumlah = 300   // <--- hanya bisa di akses di dalam scope
    console.log(jumlah) // <--- output 300
}  // <--- akhir scope
console.log(jumlah) // <--- output 500

const harga = 500 
if (true){  // <--- awal scope
    const harga = 300   // <--- hanya bisa di akses di dalam scope
    console.log(harga) // <--- output 300
}  // <--- akhir scope
console.log(harga) // <--- output 500


//============ REASSIGNED & REDECLARED ====================
// 1. VAR
// Tidak akan keluar pesan error ketika terjadi duplikasi. Sangat riskan

var name = 'bot' // <--- declare & assign
console.log(name);
var name = 'bot sabrina' // <--- redeclare & reassign
console.log(name)

// 2. LET
// Bisa di Reassign tapi tidak dapat di Redeclare

let namaTeman = 'budhi'  // <--- declare & assign
namaTeman = 'sabrina'  // <--- reassign
console.log(namaTeman);  // <--- output "sabrina"

// let namaTeman = 'chandra'         // <--- redeclare
// console.log(namaTeman);           // pasti ERROR. Coba buka comment di line 64 dan 65 untuk mencoba. 

// 3. CONST
// Karena Const bersifat immutable berarti value/ datanya tidak dapat di ubah
// Tidak bisa di reassign atau redeclare

const tahu = 'bulat'        // <--- Declare & Assign
// tahu = 'kotak'      // <--- Reassign
// const tahu = 'bandung'     // <--- Redeclare
// console.log(tahu);       // <--- pasti ERROR. Coba buka comment di line 72 sampai 74 untuk mencoba.

        // Tapii tidak berlaku jika valuenya Object dan Array
        // Object dan Array bersifat mutable

        const obj = { kota: 'Bekasi', kecamatan: 'Margahayu'}   // <--- Value object
        obj.negara = 'Indonesia'    // <--- Reassign
        console.log(obj)    // <--- Muncul tambahan indonesia

        //obj={}      // <--- Redeclare
        // pasti ERROR. Coba buka comment di line 83 untuk mencoba.
        // Tidak dapat di Redeclare


 