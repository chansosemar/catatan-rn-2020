//====================== CONDITIONAL =================== 1

// 1. OPERATOR


//  ==      -->     Equal to ("Value dan Tipe Data sama =true")
//                           ("Value sama dan Tipe Data beda =true")

//  ===     -->     Equal value and type ("Value dan Tipe Data sama =true")
//                                       ("Value sama dan Tipe Data beda =false")

//  !=      -->     Not equal ("Value beda =true")
//                            ("Value sama =false")

//  !==     -->     Not equal value or type   ("Value beda / Tipe data beda =true")
//                                            ("Value sama / Tipe data sama =false")
                                          
//  &&      -->     AND     ("Jika dua perbandingan nilainya true = true")
//                          ("Jika dua perbandingan hanya satu yg true = false")
                        
//  ||      -->     OR      ("Jika perbandingan salah satunya nilai true = true")
//                          ("Jika perbandingan semua nilai false = false")



// 2. IF

// var lapar;      // <--- declare lapar
// lapar = prompt('apakah kamu lapar?');       // <--- assign lapar dengan prompt (fungsi JS yg memunculkan pop up pada browser)
// if (lapar == 'iya') {       // <--- Jika jawaban dari lapar adalah "iya" (true menurut operator == )
// console.log('saya butuh makan);     // <--- sfa
// }


var nilai1 = [1,20000,3,4,5,6];
var nilai2 = [22,80,12,45,45];
var nilai3 = [2,70,5,43,5,5]
var sambung = nilai1.concat(nilai2, nilai3);
var hasilMax = Math.max(... sambung);
console.log(hasilMax); 


